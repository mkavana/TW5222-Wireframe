#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask, render_template, send_file
app = Flask(__name__)


'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |                           R O U T E S
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
@app.route('/')
def index():
    return render_template('index.html', title='Home')

# Lessons routes
@app.route('/lesson-1')
def lesson_1():
    return render_template('lesson_1.html', title='Lesson 1')

@app.route('/lesson-2')
def lesson_2():
    return render_template('lesson_2.html', title='Lesson 2')

@app.route('/lesson-3')
def lesson_3():
    return render_template('lesson_3.html', title='Lesson 3')

@app.route('/lesson-4')
def lesson_4():
    return render_template('lesson_4.html', title='Lesson 4')

@app.route('/lesson-5')
def lesson_5():
    return render_template('lesson_5.html', title='Lesson 5')

@app.route('/lesson-6')
def lesson_6():
    return render_template('lesson_6.html', title='Lesson 6')

@app.route('/lesson-7')
def lesson_7():
    return render_template('lesson_7.html', title='Lesson 7')

@app.route('/lesson-8')
def lesson_8():
    return render_template('lesson_8.html', title='Lesson 8')

# static page routes
@app.route('/faq')
def faq():
    return render_template('faq.html', title='FAQ')

@app.route('/glossary')
def gloassary():
    return render_template('glossary.html', title='Glossary')

@app.route('/references')
def references():
    return render_template('references.html', title='References')

@app.route('/map')
def map():
    return render_template('map.html', title='Sitemap')

@app.route('/resources')
def resources():
    return render_template('resources.html', title='Useful resources')

@app.route('/pdf')
def pdf():
    return render_template('pdf.html', title='Printable PDF')

# pdf file routes
@app.route('/pdf/lesson-1')
def return_file1():
    return send_file ('static/pdf/lesson-1.pdf' , attachment_filename='pdf/lesson-1.pdf')

@app.route('/pdf/lesson-2')
def return_file2():
    return send_file ('static/pdf/lesson-2.pdf' , attachment_filename='pdf/lesson-2.pdf')

@app.route('/pdf/lesson-3')
def return_file3():
    return send_file ('static/pdf/lesson-3.pdf' , attachment_filename='pdf/lesson-3.pdf')

@app.route('/pdf/lesson-4')
def return_file4():
    return send_file ('static/pdf/lesson-4.pdf' , attachment_filename='pdf/lesson-4.pdf')

@app.route('/pdf/lesson-5')
def return_file5():
    return send_file ('static/pdf/lesson-5.pdf' , attachment_filename='pdf/lesson-5.pdf')

@app.route('/pdf/lesson-6')
def return_file6():
    return send_file ('static/pdf/lesson-6.pdf' , attachment_filename='pdf/lesson-6.pdf')

@app.route('/pdf/lesson-7')
def return_file7():
    return send_file ('static/pdf/lesson-7.pdf' , attachment_filename='pdf/lesson-7.pdf')

@app.route('/pdf/lesson-8')
def return_file8():
    return send_file ('static/pdf/lesson-8.pdf' , attachment_filename='pdf/lesson-8.pdf')

'''
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    |
    |                           R U N    A P P
    |
    """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
'''
if __name__ == '__main__':
    app.run()                         # REMOTE

# if __name__=='__main__':
#     app.run(debug=True)                 # LOCAL