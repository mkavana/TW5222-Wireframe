# TW5222: First Flask App on PythonAnywhere Course - README

----
## What is the 'First Flask App on PythonAnywhere Course'?
see [LiveDemo](http://firstflaskapp.pythonanywhere.com/)

> This is a simple Flask-based, wireframe design proposal for an online e-learning course.  It is hosted on PythonAnywhere and built upon the [Heroic Features](https://startbootstrap.com/template-overviews/heroic-features) bootstrap template".

----
## Requirements
You should have Python (version 3) installed on your computer, [download Python 3](https://www.python.org/).  You should also [install the Git version control system](https://git-scm.com/).  

Linux (Debian) users can install Git from a terminal with:
    
    $ sudo apt install git

You will also require a Python package called 'virtualenv'.  You can read about virtualenv [here](https://iamzed.com/2009/05/07/a-primer-on-virtualenv/).  Linux (Debian) users can install the virtualenv package from a terminal with:
    
    $ sudo apt install virtualenv

----
## Usage
The following instructions require you to enter commands into a terminal.  These instructions are specific to Linux (Debian), but may work with other operating systems.  

### Set up a container directory for the application/ project files
Using a terminal, make a new directory called 'somefoldername' which will hold a copy of the application files:

    $ mkdir somefoldername
	
Change into your newly created directory:
    
    $ cd somefoldername

### Clone our application and use Git version control
You can get a copy of the contents of our GitLab repository by using Git to 'clone' a copy of our application files into your new 'somefoldername' directory.  Note that cloning will create a new folder called 'TW5222-Wireframe'inside your 'somefoldername' directory, which will contain a copy of the contents of our git repository.  Changes you make to the codebase will be tracked in Git.  If you do not wish to track changes delete the .git directory from inside your project folder.  

     $ git clone https://gitlab.com/mkavana/TW5222-Wireframe.git

### Create and activate a new virtual environment
To avoid interfering with the version of Python used by your system, you can make a virtual copy of Python for use with this project.  When you delete the project files, you can delete the virtual copy of Python too, without impacting your system.  Use 'virtualenv' to create a new virtual environment called 'venv' inside the 'somefoldername' directory.  Issue the following from inside your 'somefolder' name directory:

    $ virtualenv venv

#### Directory structure
Your container directory should now have the following directory structure:
```sh
somefoldername/
|
|--TW5222-Wireframe/
|   |--static/
|   |--templates/
|   |--app.py
|   |--README.md
|   |--.git/        [hidden folder]
|
|--venv
```

#### Activate the new virtual environment
Change into 'bin' directory, inside your new 'venv' directory:

    $ cd venv/bin		

Activate the newly created virtual environment 'venv':
    
    $ source activate

### Install Flask
Use 'pip' to install the Flask module for Python into your newly created virtual environment 'venv':

    (venv) pip install flask

### Running the application
Change into the project's root directory (i.e. 'somefoldername'):

    $ cd ~/path/to/somefoldername

Change into the 'TW5222-Wireframe' directory, which was created when you cloned the repository:

    $ cd TW5222-Wireframe

Run the file 'app.py' in Python:

    $ python ./app.py

### View the Web application
When you see the running message (below), open a web browser and visit the URL 'http://127.0.0.1:5000'

    '* Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)'

### Closing the Web applcation
To close the application, press CTRL + c from inside the terminal window that you ran the app from.  

You may also wish to deactivate the virtual environment you are running application inside of (see next section).  

### Deactivating and activating a virtual environment
Deactivate a running virtual environment with:
    
    (venv) deactivate

To re-activate a virtual environment, change into the project's 'venv/bin' directory:

    $ cd the/path/to/somefoldername/venv/bin

Run 'source activate':

    ~:/the/path/to/somefoldername/venv/bin$ source activate

----

----
## thanks
* [markdown-js](https://github.com/evilstreak/markdown-js)
* [Heroic Features](https://startbootstrap.com/template-overviews/heroic-features)
* [Slides](https://www.slides.com)
* [goConqr](https://www.goconqr.com)
* [Python Anywhere](https://www.pythonanywhere.com)
* [Flask](https://flask.pocoo.orgs)